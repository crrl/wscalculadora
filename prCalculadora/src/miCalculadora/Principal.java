package miCalculadora;

public class Principal {
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		 Calculadora c = new Calculadora();
		System.out.println("Calculadora aritmetica.");
		System.out.println("=======================");
		System.out.println("sumando 2+3 = "+ c.suma(2,3));
		System.out.println("restando 10-4= "+c.resta(10, 4));
		System.out.println("Multiplicando 4 * 3 = "+c.multiplica(4, 3));
		System.out.println("Dividiendo 4 /2 = "+c.divide(4, 2));
	}

}
