package miTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SumaYResta.class, testMultiplicaYDivide.class, testResta.class })
public class AllTests {

}
