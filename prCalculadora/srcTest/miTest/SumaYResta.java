package miTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import miCalculadora.Calculadora;

public class SumaYResta {
	Calculadora c;	
	@Before
	public void setUp() throws Exception {
		c = new Calculadora();
	}

	@Test
	public void testSuma() {
		assertTrue(c.suma(2, 3)==5);	
	}
	
	@Test
	public void testResta(){
		assertTrue(c.resta(10,4)==6);
	}

}
