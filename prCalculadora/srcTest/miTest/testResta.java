package miTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import miCalculadora.Calculadora;

public class testResta {
Calculadora c;
	@Before
	public void setUp() throws Exception {
		c = new Calculadora();
	}

	@Test
	public void TestResta() {
		assertTrue(c.resta(5, 3)==2);
	}

}
