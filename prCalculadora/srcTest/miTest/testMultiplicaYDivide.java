package miTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import miCalculadora.Calculadora;

public class testMultiplicaYDivide {
	Calculadora c;
	@Before
	public void setUp() throws Exception {
		 c = new Calculadora();
	}

	@Test
	public void testMultiplica() {
		assertTrue(c.multiplica(3, 4)==12);
	}
	@Test public void testDivide(){
		assertTrue(c.divide(4, 2)==2);
	}

}
